package transformations;

public enum Time_Granularity {
    HOUR,
    DAY,
    MONTH,
    YEAR
}

package transformations;

import file.Log;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Generalisation {

    public static void generalise(Log log, String attribute, Object input, String value) {

        int index = log.records.get(0).indexOf(attribute);
        String[] values_to_remove = new String[1];

        if (input instanceof String) {
            values_to_remove[0] = (String) input;
        } else {
            values_to_remove = (String[]) input;
        }

        int count = 0;
        int total_cases=0;
        Set<String> unique_cases = new HashSet<String>();
        for (List<String> record : log.records) {
            if (record == log.records.get(0)) continue;

            for (String value_delete: values_to_remove)
                if (record.get(index).equals(value_delete)) {
                    record.set(index, value);
                    count++;
                    unique_cases.add(record.get(0));
                }
        }
        total_cases = unique_cases.size();

        System.out.println("----------------");
        System.out.println(String.format("GENERALISATION of %s", Arrays.toString(values_to_remove)));
        System.out.println("----------------");
        System.out.println(String.format("Generalised %s to [%s] - %d records and %d cases affected.", Arrays.toString(values_to_remove), value, count,total_cases));

        log.write_log(String.format("Generalisation - %s", Arrays.toString(values_to_remove)));
    }

    public static void generalise(Log log, String attribute, Time_Granularity TIME) {

        int index = log.records.get(0).indexOf(attribute);
        int count = 0;

        for (List<String> record:log.records) {
            if (record == log.records.get(0)) continue;

            switch (TIME) {
                case HOUR:
                    record.set(index, record.get(index).replaceFirst(":..:......", ":00:00.000"));
                    break;
                case DAY:
                    record.set(index, record.get(index).replaceFirst("..:..:......", "00:00:00.000"));
                    break;
                case MONTH:
                    record.set(index, record.get(index).replaceFirst("/.. ..:..:......", "/01 00:00:00.000"));
                    break;
                case YEAR:
                    record.set(index, record.get(index).replaceFirst("../.. ..:..:......", "01/01 00:00:00.000"));
                    break;
            }
            count++;
        }

        System.out.println("----------------");
        System.out.println(String.format("GENERALISATION of %s", TIME.toString()));
        System.out.println("----------------");
        System.out.println(String.format("Generalised %s - %d records affected.", TIME.toString(), count));

        log.write_log("Generalisation - " + TIME.toString());
    }

    public static void generalise(Log log, String attribute, Time_Granularity TIME, String activity, Object input) {
        int index = log.records.get(0).indexOf(attribute);
        int index_activity = log.records.get(0).indexOf(activity);
        String[] values_to_generalise = new String[1];

        if (input instanceof String) {
            values_to_generalise[0] = (String) input;
        } else {
            values_to_generalise = (String[]) input;
        }

        Set<String> unique_cases = new HashSet<String>();
        int count = 0;
        int total_cases = 0;
        for (List<String> record:log.records) {
            if (record == log.records.get(0)) continue;

            for (String value: values_to_generalise)
            {
                if (record.get(index_activity).equals(value)) {
                    switch (TIME) {
                        case HOUR:
                            record.set(index, record.get(index).replaceFirst(":..:......", ":00:00.000"));
                            break;
                        case DAY:
                            record.set(index, record.get(index).replaceFirst("..:..:......", "00:00:00.000"));
                            break;
                        case MONTH:
                            record.set(index, record.get(index).replaceFirst("/.. ..:..:......", "/01 00:00:00.000"));
                            break;
                        case YEAR:
                            record.set(index, record.get(index).replaceFirst("../.. ..:..:......", "01/01 00:00:00.000"));
                            break;
                    }
                    unique_cases.add(record.get(0));
                    count++;
                }
            }
        }

        total_cases = unique_cases.size();

        System.out.println("----------------");
        System.out.println(String.format("GENERALISATION of %s", TIME.toString()));
        System.out.println("----------------");
        System.out.println(String.format("Generalised %s - %d records and %d cases affected.", TIME.toString(), count, total_cases));

        log.write_log("Generalisation - " + TIME.toString());

    }

}

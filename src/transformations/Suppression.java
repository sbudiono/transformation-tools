package transformations;
import file.Log;
import java.util.*;

public class Suppression {

    public static void suppress(Log log, String attribute, int k) {

        int index = log.records.get(0).indexOf(attribute);
        int index_case_id = log.records.get(0).indexOf("Case ID");
        Set<String> unique_cases = new HashSet<String>();

        // Form hashset of unique values
        Set<String> unique_values = new HashSet<String>();
        for(List<String> record: log.records) {
           if (record == log.records.get(0)) continue;
           unique_values.add(record.get(index));
        }

        // Create list of unique values, counts and case_ids
        List<String> uniques = new ArrayList<>(unique_values);
        uniques.sort(String::compareToIgnoreCase);
        int[] counts = new int[uniques.size()];
        List<HashSet<String>> case_ids = new ArrayList<HashSet<String>>();

        // Initialise hashset of case ids containing attribute values
        for(int i = 0; i <unique_values.size(); i++){
            case_ids.add(new HashSet<>());
        }

        // Count case ids
        for(List<String> record: log.records) {
            if (record == log.records.get(0)) continue;

            int uniques_index = uniques.indexOf(record.get(index));
            HashSet<String> cases = case_ids.get(uniques_index);

            if(!cases.contains(record.get(index_case_id))) counts[uniques_index] += 1;
            cases.add(record.get(index_case_id));
        }

        // Display attribute information
        System.out.println("----------------");
        System.out.println("SUPPRESSION K=" + k);
        System.out.println("----------------");
        System.out.println("Attribute: Case Count");
        for (int i = 0; i < counts.length; i++) System.out.println(uniques.get(i) + ": " + counts[i]);
        System.out.println("----------------");

        int total_cases = 0;
        int total_events = 0;


        // Suppress values
        for(int i = 0; i <counts.length; i++) {
            if(counts[i] < k) {
                int count = 0;
                for (List<String> record : log.records) {
                    if (record == log.records.get(0)) continue;
                    if (record.get(index).equals(uniques.get(i))) {
                        record.set(index, "");
                        unique_cases.add(record.get(0));
                        count++;
                    }
                }
                System.out.println(String.format("Suppressed [%s] - %d cases & %d events affected.", uniques.get(i), counts[i], count));
                total_events += count;
            }
        }
        total_cases = unique_cases.size();

        System.out.println(String.format("%d Total Cases Affected & %d Total Events Affected",total_cases,total_events));
        log.write_log("Suppression - " + k);

    }

    public static void suppress(Log log, int first, int last, int k) {

        Set<String> unique_cases = new HashSet<String>();

        // Form hashset of unique values
        HashSet<List<String>> unique_values = new HashSet<>();
        for(List<String> record: log.records) {
            if (record == log.records.get(0)) continue;
            unique_values.add(record.subList(first,last));
        }

        // Create list of unique values, counts and case_ids
        List<List<String>> uniques = new ArrayList<List<String>>(unique_values);

        int[] counts = new int[uniques.size()];
        List<HashSet<String>> case_ids = new ArrayList<HashSet<String>>();

        // Initialise hashset of case ids containing attribute values
        for(int i = 0; i <unique_values.size(); i++){
            case_ids.add(new HashSet<>());
        }

        // Count case ids
        for(List<String> record: log.records) {
            if (record == log.records.get(0)) continue;

            int uniques_index = uniques.indexOf(record.subList(first,last));
            HashSet<String> cases = case_ids.get(uniques_index);

            if(!cases.contains(record.get(0))) counts[uniques_index] += 1;
            cases.add(record.get(0));
        }

        // Display attribute information
        System.out.println("----------------");
        System.out.println("SUPPRESSION K=" + k);
        System.out.println("----------------");
        System.out.println("Attribute: Case Count");
        for (int i = 0; i < counts.length; i++) {
            if (counts[i] > 0){
                System.out.println(uniques.get(i) + ": " + counts[i]);
            }
        }
        System.out.println("----------------");

        int total_cases = 0;
        int total_events = 0;

        // Suppress values
        for(int i = 0; i <counts.length; i++) {
            List<String> list_match = new ArrayList<>(uniques.get(i));
            if(counts[i] < k) {
                int count = 0;
                for (List<String> record : log.records) {
                    if (record == log.records.get(0)) continue;

                    if (record.subList(first,last).equals(list_match)) {
                        for (int j=1; j<record.size(); j++) {
                            record.set(j,"");
                            unique_cases.add(record.get(0));
                        }
                        count++;
                    }
                }
                System.out.println(String.format("Suppressed [%s] - %d cases & %d events affected.", list_match, counts[i], count));
                total_events += count;
            }
        }
        total_cases = unique_cases.size();

        System.out.println(String.format("%d Total Cases Affected & %d Total Events Affected",total_cases,total_events));
        log.write_log("Suppression - " + k);
    }
}

package file;

import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

public class Log {

    public List<List<String>> records = new ArrayList<List<String>>();
    public String filename;

    public Log(String filename) {
        this.filename = filename;

        try (CSVReader csvReader = new CSVReader(new FileReader(filename))) {
            String[] values;
            while ((values = csvReader.readNext()) != null) {
                records.add(Arrays.asList(values));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }


        Set<String> unique_cases = new HashSet<String>();
        for(List<String> record: records) {
            unique_cases.add(record.get(0));
        }

        System.out.println("Filename: " + filename);
        System.out.println("Number of Records: " + (records.size()-1));
        System.out.println("Number of Cases: " + (unique_cases.size()-1));
        System.out.println("Number of Attributes: " + records.get(0).size());

    }

    public void write_log(String extension) {
        try (CSVWriter csvWriter = new CSVWriter(new FileWriter(this.filename.substring(0,this.filename.
                length()-4)+" - " + extension + ".csv"))) {
            for (List<String> record: this.records) {
                csvWriter.writeNext(record.toArray(new String[record.size()]));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}


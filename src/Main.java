import file.Log;
import transformations.Time_Granularity;
import static transformations.Suppression.suppress;
import static transformations.Generalisation.generalise;
import static transformations.Time_Granularity.*;


public class Main {

    public static void main(String[] args) {

        String filename = "Sepsis Cases - Event Log.csv";
//        String filename = "Sepsis Cases - ER Registration.csv";
//        String filename = "Sepsis Cases - ER Registration events.csv";
//        String filename = "Hospital Billing - Event Log.csv";
//        String filename = "bpic11_hospital_log_1.01.06_31_03_06_started.csv";
//        String filename = "bpic11_hospital_log_1.01.06_30_06_06_started.csv";
//        String filename = "hospital_log.csv";
        Log log = new Log(filename);

//        // Suppress records by minimum activity count
//        suppress(log, "Activity", 1000);

//        // Generalise all timestamps
//        generalise(log, "Complete Timestamp", HOUR);
//
//        // Generalise timestamps depending on activity
//        String[] activities = {"ER Registration", "Admission NC"};
//        generalise(log, "Complete Timestamp", HOUR, "Activity", activities);
//
//        // Generalise single value
//        generalise(log, "Activity", "Release A", "Release");
//
//        // Generalise multiple values
//        String[] activities = {"Release A", "Release B", "Release C", "Release D", "Release E"};
//        generalise(log, "Activity", activities, "Release");

    }
}

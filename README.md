# Transformation Tools #

Data transformation tools for the application of privacy-preservation data mining techniques on process mining logs.

### Supported Techniques ###

* Suppression
* Generalisation

### Valid Inputs ###

* CSV file

### Dependencies ###

The CSV log file is read using OpenCSV, a simple CSV parser for Java. For Maven supported IDEs, this library can be downloaded by searching 'com.opencsv:opencsv:4.0'.

Otherwise, below are the required jar file dependencies. They can be found under /dependencies.

* commons-beanutils-1.9.3.jar
* commons-collections-3.2.2.jar
* commons-lang3-3.6.jar
* commons-loggging-1.2.jar
* commons-text-1.1.jar
* opencsv-4.0.jar

### General Method ###

1. Create 'Log' object by passing through the name of the CSV log file as a parameter.
2. Call the appropriate anonymisation method.

### Examples ###

#### Suppression - single attribute ####

suppress(log, attribute, k);

Output: <filename> - Suppression - <k>.csv

#### Suppression - single attribute ####

suppress(log, attribute, k);

Output: <filename> - Suppression - <k>.csv


#### Suppression - multiple attributes ####

Suppress observations by considering multiple attributes.
Variables first and last are indexes (0-based).
Considers all attributes from index first to index last.

suppress(log, first, last, k);

Output: <filename> - Suppression - <k>.csv

#### Generalise - timestamps ####

Time attribute must be in format dd/MM/yyyy HH:mm:ss AM/PM
Variable attribute is the name of the time attribute.
Variable time_granularity takes arguments HOUR/DAY/MONTH/YEAR.

generalise(log, attribute_time, time);

Output: <filename> - Generalisation - <time>.csv

#### Generalise - timestamps by activity ####

Generalise timestamps associated with specific activities.

Variable activities is an array of activities which you want to generalise for.

generalise(log, attribute_time, time, attribute_activity, activity_list);

Output: <filename> - Generalisation - <time>.csv

#### Generalise - single value ####

Replace a specific value with another.

generalise(log, attribute, input, value);

Output: <filename> - Generalisation - [<input>].csv

#### Generalise - multiple values ####

Replace multiple specific values with the same value.

generalise(log, attribute, activity_list, value);
Output: <filename> - Generalisation - [<activity_list>].csv
